﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class Score : MonoBehaviour {

	public Text text;

	private int score;

	// Use this for initialization
	void Update () {
		DrowScore();
	}

	public void SumScore (int quantity) {
		score += quantity;
	}

	private void DrowScore () {
		text.text = "Score: " + score;
	}
}

﻿using UnityEngine;
using System.Collections;

public class FoodGenerator : MonoBehaviour {

	public GameObject prefabFood;

	public bool foodOnScene = false;

	void Update () {
		if (foodOnScene == false) {
			Instantiate(prefabFood, FoodPosition(), Quaternion.identity);
			foodOnScene = true;
		}
	}

	Vector3 FoodPosition () {
		Vector3 pos = new Vector3(0,0,0);
		pos.x = Random.Range(-25, 26);
		pos.y = Random.Range(-18, 17);
		return pos;
	}

}

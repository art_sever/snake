﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DeathScreen : MonoBehaviour {

	public Image deadScreen;
	public GameObject gameOverPanel;

	void Start () {
		deadScreen.rectTransform.sizeDelta = new Vector2(Screen.width, Screen.height);
	}

	public void GameOverPanelActive () {
		gameOverPanel.SetActive(true);
	}

	public IEnumerator DeadScreen () {
		deadScreen.gameObject.SetActive(true);
		yield return new WaitForSeconds(0.1f);
		deadScreen.gameObject.SetActive(false);
		yield return new WaitForSeconds(0.1f);
		deadScreen.gameObject.SetActive(true);
		yield return new WaitForSeconds(0.1f);
		deadScreen.gameObject.SetActive(false);
		yield return new WaitForSeconds(0.1f);
		deadScreen.gameObject.SetActive(true);
		yield return new WaitForSeconds(0.1f);
		deadScreen.gameObject.SetActive(false);
		yield return new WaitForSeconds(0.1f);
		deadScreen.gameObject.SetActive(true);
		yield return new WaitForSeconds(0.1f);
		deadScreen.gameObject.SetActive(false);
		yield return new WaitForSeconds(0.1f);
	}
}

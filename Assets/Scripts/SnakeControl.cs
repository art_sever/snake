﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SnakeControl : MonoBehaviour {

	public float range = 1f;
	public float speed = 0.1f;

	private bool isDie = false;

	public GameObject bodyPref;
	public Transform detectCollision;
	
	private GameObject go;
	private FoodGenerator fg;

	public static int tet = 5;

	public List<Transform> bodyList = new List<Transform>();

	AudioSource sound;

	public AudioClip eatSound;
	public AudioClip deathSound;
	public AudioClip moveSound;
	public AudioClip rotateSound;

	private Score score;
	private DeathScreen deadScreen;
	// Use this for initialization
	void Start () {
		deadScreen = GameObject.Find("DetectCollision").GetComponent<DeathScreen>();
		score = GetComponent<Score>();
		sound = GetComponent<AudioSource>();
		fg = GameObject.Find("FoodGenerator").GetComponent<FoodGenerator>();
		AddTail(10);
		InvokeRepeating("MoveHead", 0.5f, Difficulty.difficulty);
	}

	public void MoveHead () {
		DrowTail(transform.position);
		transform.position += transform.right;
		sound.PlayOneShot(moveSound);
	}

	void AddTail (int value) {
		for (int i = 0; i < value; i++) {
			go = Instantiate(bodyPref) as GameObject;
			go.transform.position = transform.position;
			bodyList.Add (go.transform);
		}
	}

	void DrowTail (Vector3 previousHeadPos) {
		if(bodyList.Count > 0)
		{
			bodyList[bodyList.Count - 1].position = previousHeadPos;
			bodyList.Insert(0, bodyList[bodyList.Count - 1]);
			bodyList.RemoveAt(bodyList.Count - 1);
		}
	}


	void Update () {
		if (Input.GetKeyDown(KeyCode.LeftArrow)) 
		{
			transform.Rotate(Vector3.forward, 90f);
			sound.PlayOneShot(rotateSound);
		}
		
		if (Input.GetKeyDown(KeyCode.RightArrow)) 
		{
			transform.Rotate(Vector3.forward, -90f);
			sound.PlayOneShot(rotateSound);
		}

	}

	void FixedUpdate () {
		if (isDie == false) 
		{
			DetectCollision ();
		}

	}

	void DetectCollision () {

		RaycastHit2D ray = Physics2D.Raycast(detectCollision.position, Vector2.right, 0.1f);
		if (ray.collider != null) 
		{
			if (ray.collider.tag == "Wall" || ray.collider.tag == "Tail") 
			{
				GameOver();
				isDie = true;
			}
			
			if (ray.collider.tag == "Eat") 
			{
				AddTail(1);
				score.SumScore(10);
				fg.foodOnScene = false;
				sound.PlayOneShot(eatSound);
				Destroy(ray.collider.gameObject);
			}
		}
	}

	void GameOver () {
		deadScreen.GameOverPanelActive();
		CancelInvoke("MoveHead");
		sound.PlayOneShot(deathSound);
		StartCoroutine(deadScreen.DeadScreen());
	}

}

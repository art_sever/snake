﻿using UnityEngine;
using System.Collections;

public class Difficulty : MonoBehaviour {

	public static float difficulty = 0.1f;

	float low = 0.15f;
	float middle = 0.1f;
	float hard = 0.05f;


	public void Low () {
		difficulty = low;
	}

	public void Middle () {
		difficulty = middle;
	}

	public void Hard () {
		difficulty = hard;
	}

}
